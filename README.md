# Timer

A minimal and beautiful timer for macOS.
![beautiful Timer](Screenshot.png "Screenshot of the beautiful timer")

## Functionality

You can set an amount of minutes and seconds and the app will show you visually
how much time is left. When the time runs out it plays the "Glass" system sound.

## Inspiration

The main inspiration for this app is the famous Time Timer. I've wrote that app
for a friend because she needed a nice timer for a workshop.

## Disclaimer

Some code is heavily inspired by a tutorial for CALayer by John Blanco
(https://raptureinvenice.com/ios-brownbag-view-vs-layers-including-clock-demo/).

It's definitely also not the prettiest code since I wrote it with some time
pressure

## License

MIT

## Code: Explained

It's actually pretty easy, it has only 3 classes and 1 extension.

### ViewController

This controls the main view, it starts a timer and tells the clock view to
animate the graphics.

### ClockView

This view draws the clock and animates the graphics.

### SetupViewController

This is the preferences sheet if you click on the 'Set' button
