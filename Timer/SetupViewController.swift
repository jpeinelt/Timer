//
//  SetupViewController.swift
//  Timer
//
//  Created by julius on 21/06/16.
//  Copyright © 2016 julius. All rights reserved.
//

import Cocoa


protocol PreferencesDelegate {
    func preferencesDidChange(controller: SetupViewController, mins: Int, secs: Int)
}

class SetupViewController: NSViewController {

    @IBOutlet weak var minutesTextField: NSTextField!
    @IBOutlet weak var secondsTextField: NSTextField!

    var delegate: ViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let defaults = NSUserDefaults.standardUserDefaults()
        let minutes = defaults.integerForKey("MINUTES")
        let seconds = defaults.integerForKey("SECONDS")
        minutesTextField.stringValue = String(minutes)
        secondsTextField.stringValue = String(seconds)
    }

    @IBAction func save(sender: NSButton) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let mins = minutesTextField.integerValue
        let secs = secondsTextField.integerValue

        defaults.setInteger(mins, forKey: "MINUTES")
        defaults.setInteger(secs, forKey: "SECONDS")
        defaults.synchronize()
        delegate?.preferencesDidChange(self, mins: mins, secs: secs)
        dismissViewController(self)

    }

    @IBAction func cancel(sender: NSButton) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let minutes = defaults.integerForKey("MINUTES")
        let seconds = defaults.integerForKey("SECONDS")
        minutesTextField.stringValue = String(minutes)
        secondsTextField.stringValue = String(seconds)
        dismissViewController(self)
    }
}
