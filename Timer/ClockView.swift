//
//  ClockView.swift
//  Timer
//
//  Created by julius on 21/06/16.
//  Copyright © 2016 julius. All rights reserved.
//

import Cocoa

@IBDesignable
class ClockView: NSView {

    private var updateTimer: NSTimer?
    private var hand: CAShapeLayer?
    private var time: CGFloat = 300.0

    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
    }

    override func setFrameSize(newSize: NSSize) {
        super.setFrameSize(newSize)
        self.needsDisplay = true
        self.needsLayout = true
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.layer = {
            $0.bounds = self.bounds
            $0.backgroundColor = NSColor.whiteColor().CGColor
            $0.contentsGravity = kCAGravityResizeAspectFill
            return $0
        }(CALayer())
        self.layerContentsRedrawPolicy = .DuringViewResize
        self.layer?.needsDisplayOnBoundsChange = true
    }

    func setupWith(time: CGFloat) {
        self.time = time
        drawHand()
    }

    override func layout() {
        self.layer?.bounds = self.bounds
        if let sublayers = self.layer?.sublayers {
            for l in sublayers {
                l.removeFromSuperlayer()
            }
        }
        drawNumbers()
        drawTicks()
        drawHand()
        super.layout()
    }

    override func viewDidEndLiveResize() {
        super.viewDidEndLiveResize()
    }

    private func drawHand() {
        hand?.removeFromSuperlayer()
        hand = {
            let radius = (self.bounds.width - 10) / 2.0
            let fractionOfCircle = CGFloat(time / 3600.0)
            let center = CGPointMake(CGRectGetMidX(self.bounds) - 5, CGRectGetMidY(self.bounds) - 5.0)
            let path = NSBezierPath()
            path.moveToPoint(center)
            path.appendBezierPathWithArcWithCenter(center, radius: radius, startAngle: 0, endAngle: 360.0 * fractionOfCircle, clockwise: false)
            let color = NSColor.redColor()
            $0.strokeColor = color.CGColor
            $0.opacity = 0.75
            $0.fillColor = NSColor.redColor().CGColor
            $0.bounds = CGRectMake(0, 0, self.bounds.size.width - 10.0, self.bounds.size.height - 10.0)
            $0.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
            $0.transform = CATransform3DMakeRotation((CGFloat(M_PI)) / 2, 0, 0, 1)
            $0.path = path.CGPath(forceClose: false)
            return $0
        }(CAShapeLayer())

        layer?.addSublayer(hand!)
    }

    private func drawNumbers() {
        for i in 0...11 {
            let number = CATextLayer()
            number.string = String(i*5)
            number.alignmentMode = "center"
            number.fontSize = 18.0;
            number.foregroundColor = NSColor.blackColor().CGColor
            number.bounds = CGRectMake(0.0, 0.0, 25.0, self.bounds.size.height - 15)
            number.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
            number.transform = CATransform3DMakeRotation((CGFloat(M_PI) * 2) / 12.0 * CGFloat(i), 0, 0, 1)
            layer?.addSublayer(number)
        }
    }

    private func drawTicks() {
        for i in 1...60 {
            let tick: CAShapeLayer = {
                let path = CGPathCreateMutable()
                let width: CGFloat = i % 5 == 0 ? 2.0 : 1.0
                CGPathAddEllipseInRect(path, nil, CGRectMake(0.0, 0.0, width, 5.0));
                $0.strokeColor = NSColor.blackColor().CGColor
                $0.bounds = CGRectMake(0.0, 0.0, 1.0, self.bounds.size.height / 2.0 - 5.0)
                $0.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
                $0.anchorPoint = CGPointMake(0.5, 1.0)
                $0.transform = CATransform3DMakeRotation((CGFloat(M_PI) * 2) / 60.0 * CGFloat(i), 0, 0, 1)
                $0.path = path
                return $0
            }(CAShapeLayer())
            layer?.addSublayer(tick)
        }
    }

    func startTimer() {
        updateTimer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(1.0), target: self, selector: #selector(updateHand), userInfo: nil, repeats: true)
    }

    func stopHand() {
        updateTimer?.invalidate()
    }

    func updateHand() {
        time -= 1
        drawHand()
    }
    
}
