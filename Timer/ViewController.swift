//
//  ViewController.swift
//  Timer
//
//  Created by julius on 21/06/16.
//  Copyright © 2016 julius. All rights reserved.
//

import Cocoa

enum ClockState {
    case Running
    case Stopped
}

class ViewController: NSViewController {


    @IBOutlet var clockView: ClockView!
    @IBOutlet weak var stopButton: NSButton!
    @IBOutlet weak var startButton: NSButton!
    private var seconds: Int = NSUserDefaults.standardUserDefaults().integerForKey("SECONDS")
    private var minutes: Int = NSUserDefaults.standardUserDefaults().integerForKey("MINUTES")

    private var time: CGFloat {
        get {
            return CGFloat(seconds + (60 * minutes))
        }
    }

    private var state: ClockState = .Stopped

    var timer: NSTimer?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.whiteColor().CGColor
        clockView.setupWith(time)
    }

    @IBAction func start(sender: NSButton) {
        guard state == .Stopped else {
            return
        }
        stopButton.title = "Stop"
        state = .Running
        let timeInterval = NSTimeInterval(time)
        timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: #selector(timerRunOut), userInfo: nil, repeats: false)
        clockView.startTimer()
        startButton.enabled = false
    }

    @IBAction func stopSet(sender: NSButton) {
        if state == .Running {
            timer?.invalidate()
            clockView.stopHand()
            state = .Stopped
        } else {
            performSegueWithIdentifier("PreferencesSegue", sender: self)
        }
        stopButton.title = "Set"
        startButton.enabled = true
        clockView.setupWith(time)
    }

    func timerRunOut() {
        stopButton.title = "Set"
        timer?.invalidate()
        clockView.stopHand()
        state = .Stopped
        clockView.setupWith(time)
        startButton.enabled = true
        playSound()
    }

    func playSound() {
        let sound = NSSound(named: "Glass")
        sound?.play()
        sound?.currentTime = NSTimeInterval(0)
        let delay1 = 1 * Double(NSEC_PER_SEC)
        let delay2 = 2 * Double(NSEC_PER_SEC)
        let time1 = dispatch_time(DISPATCH_TIME_NOW, Int64(delay1))
        dispatch_after(time1, dispatch_get_main_queue()) {
            // After 1 seconds
            sound?.play()
            sound?.currentTime = NSTimeInterval(0)
        }
        let time2 = dispatch_time(DISPATCH_TIME_NOW, Int64(delay2))
        dispatch_after(time2, dispatch_get_main_queue()) {
            // After 2 seconds
            sound?.play()
            sound?.currentTime = NSTimeInterval(0)
        }
    }

    override func prepareForSegue(segue: NSStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PreferencesSegue" {
            let prefVC = segue.destinationController as! SetupViewController
            prefVC.delegate = self
        }
    }
}

extension ViewController: PreferencesDelegate {
    func preferencesDidChange(controller: SetupViewController, mins: Int, secs: Int) {
        seconds = secs
        minutes = mins
        clockView.setupWith(time)
    }
}

